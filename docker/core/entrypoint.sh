#!/bin/bash
set -e


if [[ -d "/home/docker/var/cache" ]]
then
	if [ -n "$(ls -A /home/docker/var/cache)" ]
	then
		rm -rf /home/docker/var/cache/*
		chmod 777 -R /home/docker/var/cache
		echo "cache deleted"
	fi
fi

if [[ -d "/home/docker/var/logs" ]]
then
	chmod 777 -R /home/docker/var/logs
	echo "logs dir writable"
fi


case $1 in 
	"apache"):
		shift
		exec apache2ctl $@
		;;
	"symfony"):
		shift
		exec php /home/docker/bin/console $@
		;;
	"composer"):
		cd /home/docker
		exec $@
		;;
	*):
		exec "$@"
		;;
esac		
